
from future.utils import raise_from
import sys
import six
import logging
import time
import dateutil.parser
import datetime
from urllib.parse import urlparse
import bs4
import json
import requests

from searcch.importer.importer import BaseImporter
from searcch.importer.db.model import (
    Artifact,ArtifactFile,ArtifactMetadata,ArtifactRelease,User,Person,
    Importer,Affiliation,ArtifactAffiliation,Badge,ArtifactBadge,Venue,ArtifactVenue)

LOG = logging.getLogger(__name__)

class AcmDigitalLibraryImporter(BaseImporter):
    """Provides an ACM DL DOI Importer."""

    name = "acm_dl"
    version = "0.1"

    @classmethod
    def _extract_doi(cls,url,session=None):
        try:
            if not session:
                session = requests.session()
            urlobj = urlparse(url)
            #
            # The ACM DL has added a nasty chain of redirects through
            # Cloudflare to get cookies set correctly.  None of the python
            # urllibs (urllib, urllib3, requests) can handle it.  The chain is
            # doi.org -> dl.acm.org -> dl.acm.org?cookieSet=1 -> dl.acm.org,
            # and all urllibs hang until some kind of timeout or 403.
            #
            # So instead of actually following the redirects, we just glue the
            # doi.org path onto a dl.acm.org URL.  Don't know if this will work
            # in all cases, but any other solution involves custom redirect
            # handling in one of the urllib libraries, and I'm not even sure
            # what they are all getting goofed up on.  In the 403 case, you
            # would perhaps guess that they are dropping the cloudflare cookie,
            # or maybe that redirect #4 after the cookieSet redirect (#3)
            # (which identical location was already sent in #2) is causing a
            # loop detection (but that should result in exception).
            #
            if urlobj.netloc == "doi.org":
                res = None
                try:
                    res = session.get("https://dl.acm.org/doi" + urlobj.path)
                    if res.status_code == requests.codes.ok:
                        return (res,urlobj.path[1:])
                except:
                    pass
            res = session.get(url,timeout=30)
            urlobj = urlparse(res.url)
            if urlobj.netloc == "dl.acm.org":
                if urlobj.path.startswith("/doi/abs/"):
                    return (res,urlobj.path[9:])
                if urlobj.path.startswith("/doi/"):
                    return (res,urlobj.path[5:])
        except BaseException:
            LOG.exception(sys.exc_info()[1])
        return (None,None)

    @classmethod
    def can_import(cls,url):
        """Checks to see if this URL is a doi.org or dl.acm.org/doi URL."""
        (res,doi) = cls._extract_doi(url)
        if res and doi:
            return True
        return False

    def import_artifact(self,candidate,parent=None):
        """Imports an artifact from the ACM Digital Library and returns an Artifact, or throws an error."""
        url = candidate.url
        LOG.debug("importing '%s' from ACM Digital Library" % (url,))
        session = requests.session()
        (res,doi) = self.__class__._extract_doi(url,session=session)
        firstpage = res.content
        newurl = res.url
        res = session.post(
            "https://dl.acm.org/action/exportCiteProcCitation",
            data={"dois":doi,
                  "targetFile":"custom-bibtex","format":"bibTex"})
        res.raise_for_status()
        LOG.debug("ACM record: %r",res.json())
        j = res.json()["items"][0][doi]
        if "DOI" in j and j["DOI"] != doi:
            doi = j["DOI"]

        title = name = j.get("title")
        if not title:
            raise Exception("no title ACM metadata")
        description = j.get("abstract")
        if not description:
            LOG.warning("%s (%s) missing abstract",newurl,url)

        authors = []
        for a in j.get("author",[]):
            aname = "%s %s" % (a.get("given",""),a.get("family",""))
            aname = aname.strip() or None
            email = a.get("email",None)
            if not aname and not email:
                raise Exception("missing author name and email address")
            authors.append(Person(email=email,name=aname))
        affiliations = []
        for person in authors:
            LOG.debug("adding author %r",person)
            affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person),roles="Author"))

        metadata = list()
        metadata.append(ArtifactMetadata(
            name="doi",value=doi,source="acm_dl"))
        if "original-date" in j \
          and "date-parts" in j["original-date"] \
          and isinstance(j["original-date"]["date-parts"],list) \
          and len(j["original-date"]["date-parts"]) == 3:
            [year,month,day] = j["original-date"]["date-parts"]
            metadata.append(ArtifactMetadata(
                name="ctime",value="%(d)-%(02d)-%(02d)T00:00:00" % (year,month,day),
                source="acm_dl"))
        verbatim_metadata_names = [
            "collection-title","container-title","call-number","event-place",
            "ISBN","number-of-pages","page","publisher","publisher-place" ]
        for vmn in verbatim_metadata_names:
            if not vmn in j:
                continue
            metadata.append(ArtifactMetadata(
                name=vmn,value=str(j[vmn]),source="acm_dl"))
        metadata.append(ArtifactMetadata(
            name="acm_full_citation",value=json.dumps(j),
            source="acm_dl",type="text/json"))

        # See if we can extract badge metadata.
        soup = elm = None
        try:
            soup = bs4.BeautifulSoup(firstpage,"lxml")
            elm = soup.find(name="span",attrs={"class":"badges"})
        except:
            LOG.debug("no identifiable badge info")
            LOG.exception(sys.exc_info()[1])
        artifact_badges = []
        if elm:
            try:
                badges = elm.findAll(
                    name="a",attrs={"class":"img-badget"})
                for b in badges:
                    badge_url = b.get("href")
                    badge_object = self.session.query(Badge).\
                      filter(Badge.url == badge_url).\
                      filter(Badge.verified == True).first()
                    if badge_object:
                        artifact_badges.append(ArtifactBadge(badge=badge_object))
                    value = dict(
                        title=b.get("data-title"),
                        url=badge_url)
                    u = urlparse(badge_url)
                    if u and u.fragment:
                        value["shortname"] = u.fragment
                    value = json.dumps(value)
                    metadata.append(ArtifactMetadata(
                        name="badge",value=value,type="text/json",
                        source="acm_dl"))
            except:
                LOG.warn("failed to scrape existing badge info")
                LOG.exception(sys.exc_info()[1])

        record_url = j.get("URL",newurl)
        # Extract venue metadata
        soup = None
        nav = None
        typ = None
        venue_url = None
        artifact_venue = []
        venue_verified = False
        venue_type = "other"
        venue_object = None
        try:
            soup = bs4.BeautifulSoup(firstpage,"lxml")
            nav = soup.find('nav',attrs={"class":"article__breadcrumbs separator"})
            if nav:
                tmp  = nav.findAll('a')
                what = tmp[1]
                typ = what["href"]
                if typ == "/conferences":
                    venue_type = "conference"
                elif typ == "/journals":
                    venue_type = "journal"
                elif typ == "/magazines":
                    venue_type = "magazine"
                if len(tmp)>1:
                    loc = tmp[-2]["href"]
                    venue_url = "https://dl.acm.org" + loc
                else:
                    venue_url = url
            venue_object = self.session.query(Venue).\
                filter(Venue.url  == venue_url).\
                filter(Venue.verified == True).first()
        except:
            LOG.warn("failed while getting venue object")
            LOG.exception(sys.exc_info()[1])  
        try:    
            if venue_object:
                artifact_venue.append(ArtifactVenue(venue=venue_object))
                venue_verified = True
            keys = ['title' , 'abbrev' , 'url' , 'description' , 'location' , 'year' , 'month' , 'start_day' ,'end_day' , 'publisher' , 'publisher_location' , 'publisher_url' , 'isbn' , 'issn' , 'doi' , 'volume' , 'issue' , 'verified']
            value = dict.fromkeys(keys)
            value['type'] = venue_type
            value['title'] = j["container-title"]
            value['url'] = venue_url
            value['verified'] = venue_verified
            if "ISBN" in j:
                value["isbn"] = j["ISBN"]
            if "ISSN" in j:
                value["issn"] = j["ISSN"]
            if "collection-title" in j:
                value["abbrev"]  = j["collection-title"]
            if "issue" in j:
                value["issue"] = j["issue"]
            if "publisher-place" in j:
                value["publisher_location"] = j["publisher-place"]
            if "publisher" in j:
                value["publisher"] = j["publisher"]
            if "volume" in j:
                value["volume"] = j["volume"]
            if len(artifact_venue) == 0:
                artifact_venue.append(ArtifactVenue(venue = Venue(title = value['title'] , type = value['type'],abbrev = value['abbrev'], url = value['url'], description = value['description'], location = value['location'], year = value['year'], month = value['month'], start_day = value['start_day'],end_day = value['end_day'],publisher = value['publisher'], publisher_location = value['publisher_location'], publisher_url = value['publisher_url'], isbn = value['isbn'], issn = value['issn'],doi = value['doi'], volume = value['volume'],issue = value['issue'], verified = value['verified'])))
        except:
            LOG.warn("failed to scrape existing venue info")
            LOG.exception(sys.exc_info()[1])
         
        # Check to see if a PDF is available.  Note that this will fail if the
        # source IP address running this process does not have full access to
        # the DL.
        pdf_url = "https://dl.acm.org/doi/pdf/%s" % (doi,)
        res = session.head(pdf_url)
        files = []
        if res.status_code == requests.codes.ok:
            filename = "%s.pdf" % (doi,)
            idx = doi.rfind("/")
            if idx > 0:
                filename = "%s.pdf" % (doi[idx+1:],)
            files.append(
                ArtifactFile(url=pdf_url,name=filename,filetype="application/pdf"))
        else:
            LOG.info("failed to HEAD the pdf (%r)",res.status_code)

        return Artifact(
            type="publication",url=record_url,title=title,description=description,
            name=name,ctime=datetime.datetime.now(),ext_id=doi,
            owner=self.owner_object,importer=self.importer_object,
            meta=metadata,affiliations=affiliations,parent=parent,
            files=files,badges=artifact_badges,venues=artifact_venue)
