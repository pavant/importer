import logging
import datetime
import requests
from urllib.parse import urlparse
import bs4

from paperswithcode import PapersWithCodeClient
from searcch.importer.importer import BaseImporter
from searcch.importer.db.model import (
    Artifact,ArtifactFile,ArtifactMetadata,ArtifactRelease,User,Person,
    Importer,Affiliation,ArtifactAffiliation,ArtifactTag, CandidateArtifactRelationship, CandidateArtifact )

LOG = logging.getLogger(__name__)

class PapersWithCodeImporter(BaseImporter):
    """Provides an PapersWithCode Importer."""

    name = "paperswithcode"
    version = "0.1"

    @classmethod
    def _extract_record_id(self, url):
        try:
            #session = requests.session()
            res = requests.get(url)
            urlobj = urlparse(res.url)
            if urlobj.netloc == "paperswithcode.com" \
              and urlobj.path.startswith("/paper/"):
                return True
        except BaseException:
            return None

    @classmethod
    def can_import(cls, url):
        """Checks to see if this URL is a paperswithcode.com URL"""
        if cls._extract_record_id(url):
            return True
        return False

    def import_artifact(self,candidate, parent=None):
        """Imports an artifact from paperswithcode and returns an Artifact, or throws an error"""
        url = candidate.url
        LOG.debug("Importing %s from paperswithcode" % (url,))

        session = requests.session()
        res = requests.get(url)

        dataset_ids = []
        soup = bs4.BeautifulSoup(res.content, 'html.parser')
        datasets = soup.find('div', class_='paper-datasets')
        if datasets:
            dataset_links_a = datasets.find_all('a')
            for dataset_link in dataset_links_a:
                link = dataset_link.get('href')
                dataset_id = link[len("/dataset/"):].rstrip("/")
                dataset_ids.append(dataset_id)

        client = PapersWithCodeClient()
        urlobj = urlparse(res.url)
        if urlobj.netloc == "paperswithcode.com" \
                and urlobj.path.startswith("/paper/"):
            paper_id = urlobj.path[len("/paper/"):].rstrip("/")
            paper_info = client.paper_get(paper_id)
            url_pdf = paper_info.url_pdf
            url_pdf_parsing = urlparse(url_pdf)
            url_pdf_name = url_pdf_parsing.path[len("/pdf/"):].rstrip("/")
            title = paper_info.title
            abstract = paper_info.abstract

            affiliations = []
            for author in paper_info.authors:
                affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=Person(name=author)), roles="Author"))

            # Getting the repo links
            paper_repo_list = client.paper_repository_list(paper_id)
            repo_links = []
            for paper_repo_info in paper_repo_list.results:
                repo_links.append(paper_repo_info.url)

            # Getting the dataset objects
            dataset_in_papers = []
            for dataset_id in dataset_ids:
                dataset_paper = client.dataset_get(dataset_id)
                dataset_in_papers.append(dataset_paper)

            files = []

            files.append(ArtifactFile(
                url=url_pdf,
                filetype="application/pdf",
                name=url_pdf_name
            ))

            # Create candidate artifacts for repo_links and datasets
            artifact_relationships = []
            for repo_link in repo_links:
                candidate_artifact = CandidateArtifact(url=repo_link, type="software", ctime=datetime.datetime.now(), owner=self.owner_object)
                candidate_artifact_relationship = CandidateArtifactRelationship(
                    relation="describes", related_candidate=candidate_artifact)
                artifact_relationships.append(candidate_artifact_relationship)

            for dataset_paper in dataset_in_papers:
                candidate_artifact = CandidateArtifact(url=dataset_paper.url, type="dataset", ctime=datetime.datetime.now(), owner=self.owner_object)
                candidate_artifact_relationship = CandidateArtifactRelationship(
                    relation="describes", related_candidate=candidate_artifact)
                artifact_relationships.append(candidate_artifact_relationship)

            papers_with_code_artifact = Artifact(type="software", url=url, title=title, description=abstract, ctime=datetime.datetime.now(),
                            ext_id=url, affiliations=affiliations, files=files, candidate_relationships=artifact_relationships, owner=self.owner_object)

            return papers_with_code_artifact

