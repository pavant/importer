import os
import logging
import hashlib
import threading
import requests
import json
import datetime
import time
import werkzeug
import atexit
import socket

import flask
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from searcch.importer.db import (get_db_session,get_db_engine)
from searcch.importer.server.config import app_config
from searcch.importer.util.config import (
    config_section,ConfigSection )
from searcch.importer.util.config import find_configfile,get_config_parser
import searcch.importer.util.retrieve
from searcch.importer.importer import get_importer
from searcch.importer.exporter import get_exporter

#
# Read our config in our style, then forward necessary bits into Flask.
#
@config_section
class FlaskConfigSection(ConfigSection):

    @classmethod
    def section_name(cls):
        return "flask"

    @classmethod
    def section_defaults(cls):
        return dict(
            config_name="development")

@config_section
class ServerConfigSection(ConfigSection):

    @classmethod
    def section_name(cls):
        return "server"

    @classmethod
    def section_defaults(cls):
        return dict(
            secret_key="",
            myurl="",
            max_tasks="1",
            remote_register="true",
            remote_update="true",
            remote_update_interval="10",
            remote_delete_on_exit="false")

cf = find_configfile()
config = get_config_parser()
if cf:
    config.read(cf)
config.read_env()
# Set a random secret for ourselves if one is not already set.
if not config["server"]["secret_key"]:
    config["server"]["secret_key"] = hashlib.sha256(os.urandom(32)).hexdigest()
# Construct a URL for ourselves if unset.
if not config["server"]["myurl"]:
    config["server"]["myurl"] = "http://%s/v1" % (socket.gethostname(),)

# Create the app and initialize its Flaskish config from our defaults.
app = Flask(__name__)
config_name = os.getenv("FLASK_ENV",None)
if config_name:
    config["flask"]["config_name"] = config_name
app.config.from_object(app_config[config["flask"].get("config_name","development")])
if os.getenv('FLASK_INSTANCE_CONFIG_FILE'):
    app.config.from_pyfile(os.getenv('FLASK_INSTANCE_CONFIG_FILE'))

# Forward some values into the flask config.
for (k,v) in config["flask"].items():
    app.config[k.upper()] = v
app.config["SECRET_KEY"] = config["server"]["secret_key"]
if config["db"]["url"]:
    app.config["SQLALCHEMY_DATABASE_URI"] = config["db"]["url"]

# Make sure our database has a schema, modulo config.
get_db_session(config=config)

db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)

if "DEBUG" in app.config and app.config["DEBUG"]:
    @app.before_request
    def log_request_info():
        app.logger.debug("Headers: %r", flask.request.headers)
        app.logger.debug("Body: %r", flask.request.get_data())


from searcch.importer.server.resources.status import (
    StatusResourceRoot)
from searcch.importer.server.resources.artifact_import import (
    ArtifactImportResourceRoot, ArtifactImportResource )

approot = app.config["APPLICATION_ROOT"]

api.add_resource(
    StatusResourceRoot,
    approot + "/status",endpoint="api.status")

api.add_resource(
    ArtifactImportResourceRoot,
    approot + "/artifact/imports", endpoint="api.artifact_imports")
api.add_resource(
    ArtifactImportResource,
    approot + "/artifact/import/<int:artifact_import_id>", endpoint="api.artifact_import")

class RemoteBackendThread(threading.Thread):
    """
    A simple thread that (re)connects to the backend as necessary
    and sends it status updates so it knows we're alive.
    """
    def __init__(self,config,db,**kwargs):
        super(RemoteBackendThread,self).__init__(**kwargs)
        self.config = config
        self.db = db
        self._stop_flag = False
        self._importer_instance_id = None
        self._registered = False

    def run(self,*args,**kwargs):
        app.logger.debug("remote backend thread running (pid %d)",os.getpid())
        session = requests.session()
        lastpost = 0
        api_root = config["searcch"].get("api_root")
        api_key = config["searcch"].get("api_key")
        myurl = config["server"].get("myurl")
        mykey = config["server"].get("secret_key")
        max_tasks = config["server"].getint("max_tasks")
        remote_update = config["server"].getboolean("remote_update")
        remote_update_interval = config["server"].getint("remote_update_interval")
        while True:
            if self._stop_flag:
                return
            if lastpost and (time.time() - lastpost) < remote_update_interval:
                time.sleep(1)
                continue
            lastpost = time.time()

            # Try a remote update to see if we're already registered.
            if not self._registered:
                r = session.post(
                    api_root + "/importers",
                    data=json.dumps(dict(url=myurl,key=mykey,max_tasks=max_tasks)),
                    headers={ "Content-type":"application/json",
                              "X-Api-Key":api_key })
                if r.status_code == requests.codes.ok:
                    self._importer_instance_id = r.json()["id"]
                    self._registered = True
                    app.logger.debug("registered at backend (%d)",os.getpid())
                    time.sleep(2)
                else:
                    app.logger.error("failed to register at backend (%d)",os.getpid())
                    exit(1)
            if self._registered and remote_update:
                r = session.put(
                    api_root + "/importer/%d" % (self._importer_instance_id,),
                    data=json.dumps(dict(status="up")),
                    headers={ "Content-type":"application/json",
                              "X-Api-Key":api_key })
                if r.status_code != requests.codes.ok:
                    app.logger.error("failed to update status at backend %r" % (
                        r.status_code,))
                    if r.status_code == 404:
                        self._registered = False
                        self._importer_instance_id = None
                else:
                    app.logger.debug("updated status at backend (%d)",os.getpid())

    def stop(self):
        self._stop_flag = True

    def delete(self):
        if not self.config["server"].getboolean("remote_delete_on_exit") \
          or not self._registered:
            return
        api_root = config["searcch"].get("api_root")
        api_key = config["searcch"].get("api_key")
        r = requests.delete(
            api_root + "/importer/%d" % (self._importer_instance_id,),
            headers={ "Content-type":"application/json",
                      "X-Api-Key":api_key })
        if r.status_code != requests.codes.ok:
            app.logger.error("failed to delete our instance at backend")
        else:
            app.logger.debug("deleted remote instance at backend")

    def cleanup(self):
        self.stop()
        app.logger.debug("stopped remote thread")
        self.join()
        app.logger.debug("joined remote thread")
        self.delete()

#
# NB: some things we only want to do once per process.
#
is_master_server = False
if threading.current_thread() == threading.main_thread() \
  or werkzeug.serving.is_running_from_reloader():
    is_master_server = True
app.logger.info(
    "is_master_server = %s (%d)",str(is_master_server),os.getpid())

#
# Carefully launch a single thread to interact with the server.  This assumes
# that we're running using the basic flask/werkzeug server.  We have to ensure
# we're on the reloader thread if running flask/werkzeug in debug mode.
#
rthread = None
if config["server"].getboolean("remote_register") and is_master_server:
    rthread = RemoteBackendThread(config,db)
    rthread.start()
    atexit.register(rthread.cleanup)
